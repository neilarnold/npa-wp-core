<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www.neilarnold.com
 * @since      1.0.0
 *
 * @package    Npa_Wp_Core
 * @subpackage Npa_Wp_Core/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Npa_Wp_Core
 * @subpackage Npa_Wp_Core/admin
 * @author     Neil Arnold <neilparnold@gmail.com>
 */
class Npa_Wp_Core_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The options name to be used in this plugin
	 *
	 * @since  	1.0.0
	 * @access 	private
	 * @var  	string 		$option_name 	Option name of this plugin
	 */
	private $option_name = 'npawpcore';

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/npa-wp-core-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/npa-wp-core-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Add an options page under the Settings submenu
	 *
	 * @since  1.0.0
	 */
	public function add_options_page() {

		$this->plugin_screen_hook_suffix = add_options_page(
			__( 'NPA WP Core Settings', 'npa-wp-core' ),
			__( 'NPA WP Core', 'npa-wp-core' ),
			'manage_options',
			$this->plugin_name,
			array( $this, 'display_options_page' )
		);

	}

	/**
	 * Render the options page for plugin
	 *
	 * @since  1.0.0
	 */
	public function display_options_page() {
		include_once 'partials/npa-wp-core-admin-display.php';
	}

	/**
	 * Register settings for settings page
	 *
	 * @since  1.0.0
	 */
	public function register_setting() {
		add_settings_section(
			$this->option_name . '_general',
			__( 'General Settings', 'npa-wp-core' ),
			array( $this, $this->option_name . '_general_cb' ),
			$this->plugin_name
		);

		add_settings_field(
			$this->option_name . '_enable_clean_adminbar',
			__( 'Enable "Clean Admin Bar"', 'npa-wp-core' ),
			array( $this, $this->option_name . '_enable_clean_adminbar_cb' ),
			$this->plugin_name,
			$this->option_name . '_general',
			array( 'label_for' => $this->option_name . '_enable_clean_adminbar' )
		);

		register_setting( $this->plugin_name, $this->option_name . '_enable_clean_adminbar', 'intval' );


		if ( get_option( $this->option_name . '_enable_clean_adminbar' ) === false ) // Nothing yet saved
    	update_option( $this->option_name . '_enable_clean_adminbar', '1' );


	}


	public function npawpcore_general_cb() {
		echo '<p>' . __( 'Please enable/disable the settings accordingly.', 'npa-wp-core' ) . '</p>';
	}

	public function npawpcore_enable_clean_adminbar_cb() {
		$enable_clean_adminbar = get_option( $this->option_name . '_enable_clean_adminbar' );
		?>
			<fieldset>
				<label>
					<input type="checkbox" name="<?php echo $this->option_name . '_enable_clean_adminbar' ?>" id="<?php echo $this->option_name . '_enable_clean_adminbar' ?>" value="1" <?php checked( $enable_clean_adminbar, '1' ); ?>>
				</label>
			</fieldset>
		<?php
	}



}
