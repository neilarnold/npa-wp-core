<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www.neilarnold.com
 * @since      1.0.0
 *
 * @package    Npa_Wp_Core
 * @subpackage Npa_Wp_Core/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Npa_Wp_Core
 * @subpackage Npa_Wp_Core/includes
 * @author     Neil Arnold <neilparnold@gmail.com>
 */
class Npa_Wp_Core_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
