<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       www.neilarnold.com
 * @since      1.0.0
 *
 * @package    Npa_Wp_Core
 * @subpackage Npa_Wp_Core/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Npa_Wp_Core
 * @subpackage Npa_Wp_Core/includes
 * @author     Neil Arnold <neilparnold@gmail.com>
 */
class Npa_Wp_Core_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'npa-wp-core',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
