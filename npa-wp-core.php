<?php

/**
 * @link              www.neilarnold.com
 * @since             1.0.0
 * @package           Npa_Wp_Core
 *
 * @wordpress-plugin
 * Plugin Name:       NPA WP Core
 * Plugin URI:        www.neilarnold.com
 * Description:       Standard functions for all my projects.
 * Version:           1.0.0
 * Author:            Neil Arnold
 * Author URI:        www.neilarnold.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       npa-wp-core
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) { die; }

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-npa-wp-core-activator.php
 */
function activate_npa_wp_core() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-npa-wp-core-activator.php';
	Npa_Wp_Core_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-npa-wp-core-deactivator.php
 */
function deactivate_npa_wp_core() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-npa-wp-core-deactivator.php';
	Npa_Wp_Core_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_npa_wp_core' );
register_deactivation_hook( __FILE__, 'deactivate_npa_wp_core' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-npa-wp-core.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_npa_wp_core() {

	$plugin = new Npa_Wp_Core();
	$plugin->run();

}
run_npa_wp_core();
