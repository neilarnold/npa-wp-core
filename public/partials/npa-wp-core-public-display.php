<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       www.neilarnold.com
 * @since      1.0.0
 *
 * @package    Npa_Wp_Core
 * @subpackage Npa_Wp_Core/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
